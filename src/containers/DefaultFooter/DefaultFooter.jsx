import React from "react";
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import * as R from "ramda";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";


class DefaultFooter extends React.Component {

  render() {
    return (
      <footer className="pt-4 my-md-5 border-top">
        <Container>
          <Row>
            <Col>
              <small className="d-block mb-3 text-muted">© 1991-2020</small>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject("stores"),
    withRouter,
  );
  return injectContext(WrappedComponent);
}

export default applyContext(DefaultFooter);
