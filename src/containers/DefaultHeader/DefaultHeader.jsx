import React from "react";
import {withRouter, Link} from "react-router-dom";
import {inject, observer} from "mobx-react";
import * as R from "ramda";

import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

import {
  ROOT_ROUTE,
  SIGNIN_ROUTE
} from "Internal";


class DefaultHeader extends React.Component {

  render() {

    return (
      <Navbar
        bg="dark"
        variant="dark"
        expand="md"
        className="mb-5"
        collapseOnSelect
      >
        <Container>
          <Navbar.Brand as={Link} to={ROOT_ROUTE}>
            TaskList
          </Navbar.Brand>
          <Navbar.Toggle/>
          <Navbar.Collapse>
            <Nav className="mr-auto">
              <Nav.Link
                as={Link}
                to={SIGNIN_ROUTE}
              >
                Sign in
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject("stores"),
    withRouter
  );
  return injectContext(WrappedComponent);
}

export default applyContext(DefaultHeader);
