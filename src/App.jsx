import React from "react";
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import {inject, observer} from "mobx-react";
import * as R from "ramda";

import {
  ROOT_ROUTE,
  PAGE404_ROUTE,
  PAGE500_ROUTE,
  SIGNIN_ROUTE,
  Page404,
  Page500,
  DefaultLayout,
} from "Internal";


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={PAGE404_ROUTE} name="Page 404" render={props => (<Page404 {...props}/>)}/>
        <Route exact path={PAGE500_ROUTE} name="Page 500" render={props => (<Page500 {...props}/>)}/>
        <Route path={ROOT_ROUTE} render={props => (<DefaultLayout {...props}/>)}/>
        <Redirect to={PAGE404_ROUTE}/>
      </Switch>
    </BrowserRouter>
  );
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject("stores"),
  );
  return injectContext(WrappedComponent);
}

export default applyContext(App);
