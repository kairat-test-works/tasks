import {applySnapshot, onSnapshot} from "mobx-state-tree";

import {clear, getItem, removeItem, setItem} from "Internal";


export const Storage = {
  setItem, clear, getItem, removeItem
};

export function genID() {
  return `_${Math.random().toString(36).substr(2, 9)}`;
}

export function storageAvailable(type) {
  const storage = window[type],
    x = "__storage_test__";
  try {
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there"s something already stored
      storage.length !== 0
    );
  }
}

export const keyCodes = {
  esc: 27,
  space: 32,
  enter: 13,
  tab: 9,
  up: 38,
  down: 40,
  home: 36,
  end: 35,
  n: 78,
  p: 80,
};

export function urlSearchParamsToObj(_search = "") {
  return Object.fromEntries(new URLSearchParams(_search));
}

export function omit(obj, omitKeys) {
  const result = {};
  Object.keys(obj).forEach(key => {
    if (omitKeys.indexOf(key) === -1) {
      result[key] = obj[key];
    }
  });
  return result;
}

export const persist = (name, store, options, schema = {}) => {
  let hydrated = false;

  let storage = options.storage;

  if (typeof localStorage !== "undefined" && localStorage === storage) {
    storage = Storage;
  }

  onSnapshot(store, _snapshot => {
    if (!hydrated) {
      return;
    }
    const snapshot = {..._snapshot};
    Object.keys(snapshot).forEach(key => {
      if (!schema[key]) {
        delete snapshot[key];
      }
    });
    const data = !options.jsonify ? snapshot : JSON.stringify(snapshot);
    storage.setItem(name, data);
  });

  storage.getItem(name).then(data => {
    if (data) {
      const snapshot = !options.jsonify ? data : JSON.parse(data);
      applySnapshot(store, snapshot);
      if (store.afterHydration && typeof store.afterHydration === "function") {
        store.afterHydration();
      }
      hydrated = true;
    }
  });
};

