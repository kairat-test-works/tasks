const KEY_PREFIX = "task-list";

function withPrefix(prefix, val, sep) {
  const separator = sep || ":";
  return `${prefix}${separator}${val}`;
}

export const AUTH_KEY = withPrefix(KEY_PREFIX, "auth-data");
