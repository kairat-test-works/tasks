// task status
export const TASK_STATUS_NOT_SOLVED = 0;
export const TASK_STATUS_I = 1;
export const TASK_STATUS_II = 2;
export const TASK_STATUS_III = 3;
export const TASK_STATUS_IV = 4;
export const TASK_STATUS_V = 5;
export const TASK_STATUS_VI = 6;
export const TASK_STATUS_VII = 7;
export const TASK_STATUS_VIII = 8;
export const TASK_STATUS_IX = 9;
export const TASK_STATUS_SOLVED = 10;

// other
export const NOT_ASSIGNED = "";
export const SERVER_STATUS_OK = "ok";
export const SERVER_STATUS_ERROR = "error";
