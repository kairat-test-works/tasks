import axios from "axios";
import Qs from "qs";

import {
  API_V2_URL,
  DEVELOPER,
} from "Internal";


const https = require("https");

export const API_V2_REQ = axios.create({
  baseURL: API_V2_URL,
  params: {
    developer: DEVELOPER,
  },
  paramsSerializer(params) {
    return Qs.stringify(params, {
      arrayFormat: "brackets",
      allowDots: true,
    });
  },
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  }),
});
