import React from "react";
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import * as R from "ramda";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import {
  urlSearchParamsToObj
} from "Internal";

import {
  TaskList,
  TaskCreateModal,
  TaskEditModal
} from "./index";


class Tasks extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: {},
      createModalOpened: false,
      updateModalOpened: false,
      loading: false
    };
  }

  handleRefresh(_params = {}) {
    const {
      stores: {
        taskStore,
      },
    } = this.props;

    this.setState({loading: true});

    taskStore.getAll(_params).finally(
      () => this.setState({loading: false})
    );
  }

  handleTableRowChange = (cell, row) => {
    this.setState({selected: row, updateModalOpened: true});
  };

  handleTableChange = (type, newState) => {
    if (type === "sort") {
      const {
        sortField,
        sortOrder,
        page
      } = newState;

      this.handleRefresh({
        sort_field: sortField,
        sort_direction: sortOrder,
        page
      });
    }
  };

  render() {
    const {
      location
    } = this.props;

    const {
      selected,
      createModalOpened,
      updateModalOpened,
      loading,
    } = this.state;

    const searchParams = urlSearchParamsToObj(location.search);

    return (
      <React.Fragment>
        <Row>
          <Col>
            <Button
              onClick={() => this.setState({createModalOpened: true})}
            >
              Add
            </Button>
          </Col>
        </Row>
        <Row>
          <Col className="pt-2">
            <TaskList
              onRowChange={this.handleTableRowChange}
              onTableChange={this.handleTableChange}
              loading={loading}
            />
          </Col>
        </Row>
        <TaskCreateModal
          show={createModalOpened}
          onClose={() => this.setState({createModalOpened: false})}
          onExit={() => {
          }}
        />
        <TaskEditModal
          initials={selected}
          show={updateModalOpened}
          onClose={() => this.setState({updateModalOpened: false})}
          onExit={() => {
          }}
        />
      </React.Fragment>
    );
  }
}

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject("stores"),
    withRouter,
  );
  return injectContext(WrappedComponent);
}

export default applyContext(Tasks);
