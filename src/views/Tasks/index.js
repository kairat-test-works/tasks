import React from "react";
import loadable from "@loadable/component";

import {
  Loading,
} from "Internal";


export const TaskStatus = loadable(
  () => import("./TaskStatus"),
  {
    fallback: (<Loading/>)
  }
);

export const TaskCreateModal = loadable(
  () => import("./modals/TaskCreate"),
  {
    fallback: (<Loading/>)
  }
);

export const TaskEditModal = loadable(
  () => import("./modals/TaskEdit"),
  {
    fallback: (<Loading/>)
  }
);

export const TaskList = loadable(
  () => import("./TaskList"),
  {
    fallback: (<Loading/>)
  }
);

export const Tasks = loadable(
  () => import("./Tasks"),
  {
    fallback: (<Loading/>)
  }
);
