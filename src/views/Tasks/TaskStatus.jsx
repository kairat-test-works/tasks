import React from "react";

import Badge from "react-bootstrap/Badge";


export default function TaskStatus(props) {

  return (
    <Badge variant="success">
      Done
    </Badge>
  );
}
