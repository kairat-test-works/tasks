import React from "react";
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import * as R from "ramda";

import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import overlayFactory from "react-bootstrap-table2-overlay";


class TaskList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      bsTableOptions: {
        cols: [
          {
            dataField: "id",
            text: "ID",
            sort: true
          },
          {
            dataField: "username",
            text: "Username",
            sort: true
          },
          {
            dataField: "email",
            text: "Email",
            sort: true
          },
          {
            dataField: "status",
            text: "Status",
            sort: true
          },
          {
            dataField: 'action',
            isDummyField: true,
            text: "Action",
            formatter: (cell, row, rowIdx) => (
              // eslint-disable-next-line jsx-a11y/anchor-is-valid
              <a href="#" onClick={() => props.onRowChange(cell, row, rowIdx)}>Edit</a>
            ),
          }
        ],
        defaultSorted: [{
          dataField: "username",
          order: "asc",
        }],
        pagination: {
          hideSizePerPage: true,
          paginationSize: 3,
          sizePerPage: 3,
          showTotal: true,
        },
        overlay: {
          spinner: true
        },
      }
    };
  }

  render() {
    const {
      stores: {
        taskStore: {tasks, totalCount}
      },
      onTableChange,
      loading
    } = this.props;

    const {cols, defaultSorted, pagination, overlay} = this.state.bsTableOptions;

    return (
      <BootstrapTable
        remote
        wrapperClasses="table-responsive"
        classes="table-sm table-hover table-striped"
        keyField="id"
        columns={cols}
        data={tasks}
        pagination={paginationFactory({
          ...pagination,
          totalSize: totalCount
        })}
        overlay={overlayFactory(overlay)}
        noDataIndication="No items"
        defaultSorted={defaultSorted}
        onTableChange={onTableChange}
        loading={loading}
      />
    );
  }
}

TaskList.defaultProps = {
  onRowChange: () => {
  },
  onTableChange: () => {
  }
};

function applyContext(WrappedComponent) {
  const injectContext = R.pipe(
    observer,
    inject("stores"),
    withRouter,
  );
  return injectContext(WrappedComponent);
}

export default applyContext(TaskList);
