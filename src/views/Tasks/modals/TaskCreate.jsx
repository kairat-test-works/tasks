import React from "react";
import Modal from "react-bootstrap/Modal";

import {
  CustomForm,
} from "Internal";


class TaskCreate extends React.Component {

  _fields = [
    {
      title: "Username",
      type: "text",
      control: "username",
      rules: [
        {
          required: true,
          message: "Required field",
        }
      ],
      placeholder: "",
      required: true,
    },
    {
      title: "Email",
      type: "text",
      control: "email",
      rules: [
        {
          required: true,
          message: "Required field",
        }
      ],
      placeholder: "",
      required: true,
    },
    {
      title: "Text",
      type: "textarea",
      control: "text",
      rows: 3,
      rules: [
        {
          required: true,
          message: "Required field",
        }
      ],
      placeholder: "",
      required: true,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  handleCreate = (vals) => {
    const {
      stores: {
        taskStore
      },
      onClose
    } = this.props;

    this.setState({loading: true});

    taskStore.create(vals).then(
      () => onClose(),
      () => {
      },
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      show,
      onClose,
      onExit
    } = this.props;

    const {loading} = this.state;

    return (
      <Modal
        onHide={onClose}
        onExited={onExit}
        show={show}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Create a task</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CustomForm
            fields={this._fields}
            initials={{}}
            onCancel={onClose}
            onSubmit={this.handleCreate}
            loading={loading}
          />
        </Modal.Body>
      </Modal>
    );
  }
}

export default TaskCreate;
