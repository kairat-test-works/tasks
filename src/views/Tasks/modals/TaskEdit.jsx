import React from "react";
import Modal from "react-bootstrap/Modal";

import {
  CustomForm,
  TASK_STATUS_NOT_SOLVED,
  TASK_STATUS_SOLVED,
} from "Internal";


class TaskEdit extends React.Component {

  _fields = [
    {
      title: "Text",
      type: "textarea",
      control: "text",
      rows: 3,
      rules: [
        {
          required: true,
          message: "Required field",
        }
      ],
      placeholder: "",
      required: true,
    },
    {
      title: "Status",
      type: "select",
      control: "status",
      options: [
        {label: "Not solved", value: TASK_STATUS_NOT_SOLVED},
        {label: "Solved", value: TASK_STATUS_SOLVED}
      ],
      rules: [
        {
          required: true,
          message: "Required field",
        }
      ],
      placeholder: "",
      required: true,
    },
  ];

  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  handleUpdate = (vals) => {
    const {
      stores: {
        taskStore
      },
      onClose
    } = this.props;

    this.setState({loading: true});

    taskStore.update(vals).then(
      () => onClose(),
      () => {
      },
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      show,
      initials,
      onClose,
      onExit
    } = this.props;

    const {loading} = this.state;

    return (
      <Modal
        onHide={onClose}
        onExited={onExit}
        show={show}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CustomForm
            fields={this._fields}
            initials={initials || {}}
            onCancel={onClose}
            onSubmit={this.handleUpdate}
            loading={loading}
          />
        </Modal.Body>
      </Modal>
    );
  }
}

export default TaskEdit;
