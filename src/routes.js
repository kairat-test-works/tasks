import React from "react";
import * as R from "ramda";

import {Tasks} from "Internal";


export function makeRoute(routes) {
  const mr = R.pipe(
    rts => R.map(
      R.cond([
        [R.and(R.startsWith("/"), R.endsWith("/")), r => r.slice(1, -1)],
        [R.startsWith("/"), r => r.substring(1)],
        [R.endsWith("/"), r => r.slice(0, -1)],
        [R.T, r => r]
      ]),
      rts
    ),
    R.join("/"),
    res => R.concat("/", res),
  );
  return mr(routes);
}

export const ROOT_ROUTE = makeRoute([]);
export const PAGE404_ROUTE = makeRoute(["404"]);
export const PAGE500_ROUTE = makeRoute(["500"]);
export const SIGNIN_ROUTE = makeRoute(["signin"]);

export const routes = [
  {
    key: "tasks-view",
    name: "Tasks",
    path: ROOT_ROUTE,
    exact: true,
    render: (props) => (
      <Tasks {...props}/>
    )
  },
];
