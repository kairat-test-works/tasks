import loadable from "@loadable/component";


export const CustomForm = loadable(() => import("./CustomForm"));
