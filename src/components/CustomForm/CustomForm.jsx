import React from "react";
import {createForm} from "rc-form";

import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";


class CustomForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      form,
      onSubmit,
    } = this.props;

    form.validateFields((errs, vals) => {
      if (!errs) {
        onSubmit(vals);
      }
      this.setState({validated: true});
    });
  };

  render() {
    const {
      form: {
        getFieldDecorator,
        getFieldError
      },
      fields,
      initials,
      messages,
      loading,
      onCancel,
    } = this.props;

    const {validated} = this.state;

    getFieldDecorator("id", {
      initialValue: initials.id,
    });

    return (
      <Form onSubmit={this.handleSubmit} validated={validated} noValidate>
        {fields.map((field, idx) => {
          const _fieldAs = {
            text: (<Form.Control
              type={field.type}
              placeholder={field.placeholder}
              required={field.required}
              disabled={loading}
            />),
            textarea: (<Form.Control
              as={field.type}
              rows={field.rows}
              placeholder={field.placeholder}
              required={field.required}
              disabled={loading}
            />),
            select: (<Form.Control as={field.type}>
              {field.options.map((opt, idx) => (<option value={opt.value}>{opt.label}</option>))}
            </Form.Control>),
          };
          return (
            <Form.Row key={`custom-field-${idx}`}>
              <Form.Group as={Col} controlId={field.control}>
                <Form.Label>{field.title}</Form.Label>
                {getFieldDecorator(field.control, {
                  ...field.rules,
                  initialValue: initials[field.control]
                })(_fieldAs[field.type])}
                {(getFieldError(field.control) || []).map((err, idx) => (
                  <Form.Control.Feedback type="invalid" key={`${field.control}-err-${idx}`}>
                    {err}
                  </Form.Control.Feedback>
                ))}
              </Form.Group>
            </Form.Row>
          );
        })}
        <Form.Row>
          <Col className="d-flex justify-content-between" xs={12}>
            <Button
              className="rounded-pill"
              variant="outline-secondary"
              onClick={onCancel}
              disabled={loading}
            >
              {messages.cancel}
            </Button>
            <Button
              onClick={this.handleSubmit}
              className="rounded-pill"
              variant="outline-primary"
              disabled={loading}
            >
              {messages.ok}
            </Button>
          </Col>
        </Form.Row>
      </Form>
    );
  }
}

CustomForm.defaultProps = {
  messages: {
    cancel: "Cancel",
    ok: "Save",
  },
  initials: {},
  onCancel: () => {
  },
  onSubmit: () => {
  }
};

function applyContext(WrappedComponent) {
  return createForm()(WrappedComponent);
}

export default applyContext(CustomForm);
