import {getEnv, types} from "mobx-state-tree";

import {
  DomainStore,
  AUTH_KEY,
} from "Internal";


export const AuthStore = DomainStore.named(
  "AuthStore"
).props({
  token: types.maybeNull(types.string),
}).actions(self => {

  const {localStg} = getEnv(self);

  function signIn(creds = {}, _params = {}) {
    return self.reqPost(
      "/login",
      creds,
      {params: _params},
      (resp) => {
        self.token = resp.data;
      }
    );
  }

  function signOut() {
    self.runInAction(() => {
      self.token = null;
    });
  }

  function afterCreate() {
    localStg.getItem(AUTH_KEY).then(
      (val) => {
        if (!val) {
          localStg.setItem(AUTH_KEY, JSON.stringify({
            token: null,
          }));
        }
      },
      () => {
      },
    );
  }

  return {
    signIn,
    signOut,
    afterCreate,
  };
});
