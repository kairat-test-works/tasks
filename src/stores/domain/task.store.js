import {getSnapshot, types} from "mobx-state-tree";

import {
  DomainStore,
  TaskModel,
} from "Internal";


export const TaskStore = DomainStore.named(
  "TaskStore"
).props({
  _tasks: types.optional(
    types.array(types.late(() => TaskModel)),
    []
  ),
  totalCount: 0
}).actions(self => {

  function getAll(_params = {}) {
    return self.reqGet(
      "/",
      {params: _params},
      (resp) => {
        self._tasks = resp.data.message.tasks;
        self.totalCount = parseInt(resp.data.message.total_task_count, 10);
      }
    );
  }

  function create(_data = {}, _params = {}) {
    return self.reqPost(
      "/create",
      _jsonToFormData(_data),
      {params: _params},
    );
  }

  function update(_id, _data = {}, _params = {}) {
    return self.reqPost(
      `/edit/${_id}`,
      _jsonToFormData(_data),
      {params: _params},
    );
  }

  function _jsonToFormData(_data) {
    const formData = new FormData();
    for (const [key, val] of _data) {
      formData.append(key, val);
    }
    return formData;
  }

  return {
    getAll,
    create,
    update,
  };
}).views(self => ({
  get tasks() {
    return getSnapshot(self._tasks);
  },
}));
