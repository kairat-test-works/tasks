import {BaseStore} from "Internal";


export const UIStore = BaseStore.named(
  "UIStore"
).props({
  isLoading: false
}).actions(self => {

  function setLoading(loading) {
    self.runInAction(() => {
      self.isLoading = loading;
    });
  }

  return {
    setLoading,
  };
});
