import {types} from "mobx-state-tree";

import {
  AuthStore,
  TaskStore,
  AppUIStore,
  TaskUIStore,
} from "Internal";


export const RootStore = types.model(
  "RootStore",
  {
    // domain
    authStore: types.late(() => AuthStore),
    taskStore: types.late(() => TaskStore),
    // ui
    appUIStore: types.late(() => AppUIStore),
    taskUIStore: types.late(() => TaskUIStore),
  }
);
