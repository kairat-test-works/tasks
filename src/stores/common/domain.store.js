import {getEnv, getSnapshot} from "mobx-state-tree";

import {BaseStore} from "Internal";


export const DomainStore = BaseStore.named(
  "DomainStore"
).actions(self => {

  const {apiV2} = getEnv(self);

  function reqGet(
    _service,
    _conf = {},
    onFull = () => {
    },
    onRej = () => {
    },
    onPend = () => {
    }
  ) {
    const prom = apiV2.get(_service, _conf);

    _handleActions(prom, onFull, onRej, onPend);

    return prom;
  }

  function reqPost(
    _service,
    _data = {},
    _conf = {},
    onFull = () => {
    },
    onRej = () => {
    },
    onPend = () => {
    }
  ) {
    const prom = apiV2.post(_service, _data, _conf);

    _handleActions(prom, onFull, onRej, onPend);

    return prom;
  }

  function _handleActions(
    prom,
    onSucc = () => {
    },
    onRej = () => {
    },
    onPend = () => {
    }
  ) {
    self.runInAction(() => {
      onPend();
    });

    prom.then(
      (resp) => {
        self.runInAction(() => {
          onSucc(resp);
        });
      },
      (err) => {
        self.runInAction(() => {
          onRej(err);
        });
      }
    );
  }

  return {
    reqGet,
    reqPost,
  };
}).views(self => ({
  get errs() {
    return getSnapshot(self._errs);
  }
}));
