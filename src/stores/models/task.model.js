import {types} from "mobx-state-tree";

import {
  BaseModel,
  TASK_STATUS_NOT_SOLVED,
  TASK_STATUS_I,
  TASK_STATUS_II,
  TASK_STATUS_III,
  TASK_STATUS_IV,
  TASK_STATUS_V,
  TASK_STATUS_VI,
  TASK_STATUS_VII,
  TASK_STATUS_VIII,
  TASK_STATUS_IX,
  TASK_STATUS_SOLVED,
} from "Internal";


export const TaskModel = BaseModel.named(
  "TaskModel"
).props({
  id: types.maybeNull(types.integer),
  username: types.maybeNull(types.string),
  email: types.maybeNull(types.string),
  text: types.maybeNull(types.string),
  status: types.optional(
    types.union(
      types.late(() => types.literal(TASK_STATUS_NOT_SOLVED)),
      types.late(() => types.literal(TASK_STATUS_I)),
      types.late(() => types.literal(TASK_STATUS_II)),
      types.late(() => types.literal(TASK_STATUS_III)),
      types.late(() => types.literal(TASK_STATUS_IV)),
      types.late(() => types.literal(TASK_STATUS_V)),
      types.late(() => types.literal(TASK_STATUS_VI)),
      types.late(() => types.literal(TASK_STATUS_VII)),
      types.late(() => types.literal(TASK_STATUS_VIII)),
      types.late(() => types.literal(TASK_STATUS_IX)),
      types.late(() => types.literal(TASK_STATUS_SOLVED)),
    ),
    TASK_STATUS_NOT_SOLVED
  ),
});
