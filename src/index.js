import "react-app-polyfill/ie9"; // For IE 9-11 support
import "react-app-polyfill/stable";
// import "react-app-polyfill/ie11"; // For IE 11 support

import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "mobx-react";
import loadable from "@loadable/component";

import {
  RootStore,
  Storage,
  API_V2_REQ,
  persist,
  AUTH_KEY,
} from "Internal";

import "./index.scss";
import reportWebVitals from "./reportWebVitals";


const App = loadable(
  () => import("./App"),
);

const rootStore = RootStore.create(
  {
    authStore: {},
    taskStore: {},
    appUIStore: {},
    taskUIStore: {},
  },
  {
    apiV2: API_V2_REQ,
    localStg: Storage,
  }
);

API_V2_REQ.interceptors.request.use(
  async (config) => {
    config.params.token = rootStore.authStore.token;
    return config;
  },
  (err) => Promise.reject(err),
);

API_V2_REQ.interceptors.response.use(
  (resp) => resp,
  (err) => Promise.reject(err)
);

persist(
  AUTH_KEY,
  rootStore.authStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {
    token: true,
  }
);

ReactDOM.render(
  (<React.StrictMode>
    <Provider stores={rootStore}>
      <App/>
    </Provider>
  </React.StrictMode>),
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
